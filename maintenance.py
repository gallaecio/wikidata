#!/usr/bin/env python2.7
# -*- coding:utf-8 -*-

"""Script that runs maintenance tasks on Wikidata."""

from shyde.wikidata.tasks import \
    update_software_versions


def main(argv):
    update_software_versions()


if __name__ == u"__main__":
    import sys
    main(sys.argv[1:])